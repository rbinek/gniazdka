package tb.sockets.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

public class MainFrame extends Window implements Runnable {

	private static final long serialVersionUID = 1L;
	
	static final int SERVER_PORT = 25000;
	private String serverHost = "localhost";
	private Socket socket;
	private ObjectOutputStream outputStream;
	private ObjectInputStream inputStream;
	
	private char[][] stateGame = new char[3][3];
	
	PlayPanel panel;
	
	public MyClient() {
		
		setSize(300, 300);
		setLocation(300, 0);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("Klient");
		
		panel = new PlayPanel(this, stateGame, 'x');

		
		setContentPane(panel);
		setVisible(true);
		new Thread(this).start();
	}
	
	
	public void sendMessage(int x, int y) {
		try {
			outputStream.writeObject(x);
			outputStream.writeObject(y);
			
			panel.removeMouseListener(panel);
		} catch (IOException e) {
		}
	}
	
	private void receiveMessage(int x, int y) {
		stateGame[x][y] = 'o';
		panel.addMouseListener(panel);
		repaint();
		checkGame();
	}
	
	@Override
	public void checkGame() {
		
		if ((stateGame[0][0] == stateGame[0][1] && stateGame[0][0] == stateGame[0][2] && stateGame[0][0] == 'o') ||
			(stateGame[1][0] == stateGame[1][1] && stateGame[1][0] == stateGame[1][2] && stateGame[1][0] == 'o') ||
			(stateGame[2][0] == stateGame[2][1] && stateGame[2][0] == stateGame[2][2] && stateGame[2][0] == 'o') ||
			(stateGame[0][0] == stateGame[1][0] && stateGame[0][0] == stateGame[2][0] && stateGame[0][0] == 'o') ||
			(stateGame[0][1] == stateGame[1][1] && stateGame[0][1] == stateGame[2][1] && stateGame[0][1] == 'o') ||
			(stateGame[0][2] == stateGame[1][2] && stateGame[0][2] == stateGame[2][2] && stateGame[0][2] == 'o') ||
			(stateGame[0][0] == stateGame[1][1] && stateGame[0][0] == stateGame[2][2] && stateGame[0][0] == 'o') ||
			(stateGame[2][0] == stateGame[1][1] && stateGame[2][0] == stateGame[0][2] && stateGame[2][0] == 'o'))
		{
			JOptionPane.showMessageDialog(this, "Przegrales");
		}
		
		else if ((stateGame[0][0] == stateGame[0][1] && stateGame[0][0] == stateGame[0][2] && stateGame[0][0] == 'x') ||
				(stateGame[1][0] == stateGame[1][1] && stateGame[1][0] == stateGame[1][2] && stateGame[1][0] == 'x') ||
				(stateGame[2][0] == stateGame[2][1] && stateGame[2][0] == stateGame[2][2] && stateGame[2][0] == 'x') ||
				(stateGame[0][0] == stateGame[1][0] && stateGame[0][0] == stateGame[2][0] && stateGame[0][0] == 'x') ||
				(stateGame[0][1] == stateGame[1][1] && stateGame[0][1] == stateGame[2][1] && stateGame[0][1] == 'x') ||
				(stateGame[0][2] == stateGame[1][2] && stateGame[0][2] == stateGame[2][2] && stateGame[0][2] == 'x') ||
				(stateGame[0][0] == stateGame[1][1] && stateGame[0][0] == stateGame[2][2] && stateGame[0][0] == 'x') ||
				(stateGame[2][0] == stateGame[1][1] && stateGame[2][0] == stateGame[0][2] && stateGame[2][0] == 'x'))
		{
				JOptionPane.showMessageDialog(this, "Wygrales");
		}
		
		else if ((stateGame[0][0] == 'o' || stateGame[0][0] == 'x') &&
				(stateGame[1][0] == 'o' || stateGame[1][0] == 'x') &&
				(stateGame[2][0] == 'o' || stateGame[2][0] == 'x') &&
				(stateGame[0][1] == 'o' || stateGame[0][1] == 'x') &&
				(stateGame[1][1] == 'o' || stateGame[1][1] == 'x') &&
				(stateGame[2][1] == 'o' || stateGame[2][1] == 'x') &&
				(stateGame[0][2] == 'o' || stateGame[0][2] == 'x') &&
				(stateGame[1][2] == 'o' || stateGame[1][2] == 'x') &&
				(stateGame[2][2] == 'o' || stateGame[2][2] == 'x')) 
		{
			JOptionPane.showMessageDialog(this, "Remis");
		}
		
	}
	
	
	@Override
	public void run() {
		int x,y;
		try {
			socket = new Socket(serverHost, SERVER_PORT);
			inputStream = new ObjectInputStream(socket.getInputStream());
			outputStream = new ObjectOutputStream(socket.getOutputStream());
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Polaczenie sieciowe nie moze byc utworzone");
			setVisible(false);
			dispose();
			return;
		}
		
		try {
			while (true) {
				x = (int)inputStream.readObject();
				y = (int)inputStream.readObject();
				receiveMessage(x,y);
				
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Polaczenie sieciowe dla klienta zostalo przerwane");
		   	setVisible(false);
		   	dispose();
		}
	}
}
