package tb.sockets.client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

abstract class Window extends JFrame {

	private static final long serialVersionUID = 1L;
	
	abstract void sendMessage(int x, int y);
	
	abstract void checkGame();
	
}



public class OrderPane extends JPanel implements MouseListener {

	private static final long serialVersionUID = 1L;
	
	private char[][] stateGame;
	private char mark = 's';
	private Window parent;
	
	public PlayPanel(Window p, char[][] s, char m) {
		stateGame = s;
		mark = m;
		parent = p;
	}

	private void drawCircle(Graphics g, int x, int y) {
		
		g.setColor(Color.GREEN);
		g.drawOval((getWidth()/3*x)+20, (getHeight()/3*y)+20, this.getWidth()/3-40, this.getHeight()/3-40);
		g.setColor(Color.BLACK);
	}
	
	private void drawSharp(Graphics g, int x, int y) {
		
		x = (getWidth()/3*x)+20;
		y = (getHeight()/3*y)+20;
		
		g.setColor(Color.RED);
		g.drawLine(x, y, x+this.getWidth()/3-40, y+this.getHeight()/3-40);
		g.drawLine(x, y+this.getHeight()/3-40, x+this.getWidth()/3-40, y);
		g.setColor(Color.BLACK);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawLine(getWidth()/3, 20, getWidth()/3, getHeight()-20);
		g.drawLine(2*getWidth()/3, 20, 2*getWidth()/3, getHeight()-20);
		g.drawLine(20, getHeight()/3, getWidth()-20, getHeight()/3);
		g.drawLine(20, 2*getHeight()/3, getWidth()-20, 2*getHeight()/3);
		
		for (int i =0; i < 3; i++) {
			for (int j=0; j<3; j++) {
				if (stateGame[i][j] == 'o')
					drawCircle(g,i,j);
				if (stateGame[i][j] == 'x')
					drawSharp(g,i,j);
			}
		}
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		
		int x = e.getX();
		int y = e.getY();
		int whereX, whereY;
		
		if (x < getWidth()/3) 
			whereX = 0;
		else if (x < getWidth()/3*2)
				whereX = 1;
			else whereX = 2;
		
		if (y < getHeight()/3) 
			whereY = 0;
		else if (y < getHeight()/3*2)
				whereY = 1;
			else whereY = 2;
		
		
		if (stateGame[whereX][whereY] != 'x' && stateGame[whereX][whereY] != 'o' )
		{
			stateGame[whereX][whereY] = mark;
			parent.sendMessage(whereX, whereY);
			repaint();
			parent.checkGame();
		}
		
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
