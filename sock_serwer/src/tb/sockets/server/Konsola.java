package tb.sockets.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Konsola extends Window implements Runnable {

	private static final long serialVersionUID = 1L;
	
	static final int SERVER_PORT = 25000;

	private Socket socket;
	private ObjectOutputStream outputStream;
	
	private char[][] stateGame = new char[3][3];
	
	PlayPanel panel;
	
	public MyServer() {
		
		setSize(300, 300);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		setTitle("Server");
		
		panel = new PlayPanel(this, stateGame, 'o');
		
		panel.addMouseListener(panel);
		
		setContentPane(panel);
		setVisible(true);
		new Thread(this).start();
	}
	
	@Override
	public void sendMessage(int x, int y) {
		try {
			outputStream.writeObject(x);
			outputStream.writeObject(y);
			
			panel.removeMouseListener(panel);
		} catch (IOException e) {
		}
	}
	
	private void receiveMessage(int x, int y) {
		stateGame[x][y] = 'x';
		panel.addMouseListener(panel);
		repaint();
		checkGame();
	}
	
	@Override
	public void checkGame() {
		
		if ((stateGame[0][0] == stateGame[0][1] && stateGame[0][0] == stateGame[0][2] && stateGame[0][0] == 'o') ||
			(stateGame[1][0] == stateGame[1][1] && stateGame[1][0] == stateGame[1][2] && stateGame[1][0] == 'o') ||
			(stateGame[2][0] == stateGame[2][1] && stateGame[2][0] == stateGame[2][2] && stateGame[2][0] == 'o') ||
			(stateGame[0][0] == stateGame[1][0] && stateGame[0][0] == stateGame[2][0] && stateGame[0][0] == 'o') ||
			(stateGame[0][1] == stateGame[1][1] && stateGame[0][1] == stateGame[2][1] && stateGame[0][1] == 'o') ||
			(stateGame[0][2] == stateGame[1][2] && stateGame[0][2] == stateGame[2][2] && stateGame[0][2] == 'o') ||
			(stateGame[0][0] == stateGame[1][1] && stateGame[0][0] == stateGame[2][2] && stateGame[0][0] == 'o') ||
			(stateGame[2][0] == stateGame[1][1] && stateGame[2][0] == stateGame[0][2] && stateGame[2][0] == 'o'))
		{
			JOptionPane.showMessageDialog(this, "Wygrales");
		}
		
		else if ((stateGame[0][0] == stateGame[0][1] && stateGame[0][0] == stateGame[0][2] && stateGame[0][0] == 'x') ||
				(stateGame[1][0] == stateGame[1][1] && stateGame[1][0] == stateGame[1][2] && stateGame[1][0] == 'x') ||
				(stateGame[2][0] == stateGame[2][1] && stateGame[2][0] == stateGame[2][2] && stateGame[2][0] == 'x') ||
				(stateGame[0][0] == stateGame[1][0] && stateGame[0][0] == stateGame[2][0] && stateGame[0][0] == 'x') ||
				(stateGame[0][1] == stateGame[1][1] && stateGame[0][1] == stateGame[2][1] && stateGame[0][1] == 'x') ||
				(stateGame[0][2] == stateGame[1][2] && stateGame[0][2] == stateGame[2][2] && stateGame[0][2] == 'x') ||
				(stateGame[0][0] == stateGame[1][1] && stateGame[0][0] == stateGame[2][2] && stateGame[0][0] == 'x') ||
				(stateGame[2][0] == stateGame[1][1] && stateGame[2][0] == stateGame[0][2] && stateGame[2][0] == 'x'))
		{
				JOptionPane.showMessageDialog(this, "Przegrales");
		}
		
		
		else if ((stateGame[0][0] == 'o' || stateGame[0][0] == 'x') &&
			(stateGame[1][0] == 'o' || stateGame[1][0] == 'x') &&
			(stateGame[2][0] == 'o' || stateGame[2][0] == 'x') &&
			(stateGame[0][1] == 'o' || stateGame[0][1] == 'x') &&
			(stateGame[1][1] == 'o' || stateGame[1][1] == 'x') &&
			(stateGame[2][1] == 'o' || stateGame[2][1] == 'x') &&
			(stateGame[0][2] == 'o' || stateGame[0][2] == 'x') &&
			(stateGame[1][2] == 'o' || stateGame[1][2] == 'x') &&
			(stateGame[2][2] == 'o' || stateGame[2][2] == 'x')) 
		{
			JOptionPane.showMessageDialog(this, "Remis");
		}
		
	}
	
	
	
	@Override
	public void run() {
		boolean socket_created = false;
		int x,y;
		
		try (ServerSocket server = new ServerSocket(SERVER_PORT)) {
			socket_created = true;
			
			while (true) {
				socket = server.accept();
				if (socket != null) {
					break;
				}
			}
			
		} catch (IOException e) {
			System.out.println(e);
			if (!socket_created) {
				JOptionPane.showMessageDialog(null, "Gniazdko dla serwera nie mo�e by� utworzone");
				System.exit(0);
			} else {
				JOptionPane.showMessageDialog(null, "BLAD SERWERA: Nie mozna polaczyc sie z klientem ");
			}
		}
		
		try ( ObjectOutputStream output = new ObjectOutputStream(socket.getOutputStream());
		   		  ObjectInputStream input = new ObjectInputStream(socket.getInputStream()); 
				)
			{
				outputStream = output;

				while (true) {
					x = (int)input.readObject();
					y = (int)input.readObject();
					receiveMessage(x, y);
					
				}
				
			
			} catch (Exception e) {

			}
		
		
	}
	
	
}